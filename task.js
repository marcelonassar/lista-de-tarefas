$('#salvarTarefa').on('click', criaTarefa);
$('#novaTarefa').keydown(function(tecla){
    if(tecla.keyCode == 13){
        criaTarefa()
    }
})

function criaTarefa(){
    if($('#novaTarefa') != ''){
        $('#listaTarefas').prepend(`
            <li>
            <span>${$('#novaTarefa').val()}</span>
            <button class='editarTarefa'>Editar</button>
            <button class='excluirTarefa'>Excluir</button>
            </li>
        `);
        $('#novaTarefa').val('');
    }
}

//Arrow function nao funcionou o $(this)
$('#listaTarefas').on('click', '.excluirTarefa', function() {
    $(this).parent().remove();
})

$('#listaTarefas').on('click', '.editarTarefa', function() {
    let tarefaEditada = prompt('Edite a Tarefa');
    if(tarefaEditada){
        $(this).parent().find('span').text(tarefaEditada);
    }
})

